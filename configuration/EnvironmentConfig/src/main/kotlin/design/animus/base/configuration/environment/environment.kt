package design.animus.base.configuration.environment

import kotlin.reflect.KClass
import kotlin.reflect.full.primaryConstructor
import kotlin.reflect.jvm.javaType
import kotlin.system.exitProcess

/**
 * The base Environment configuration requirement.
 *
 * Only Services should be instaniating an environment configuration.
 * These services will eventually need a cluster port.
 *
 * @author animus null
 * @since 0.1.0
 */
interface EnvironmentConfiguration

/**
 * Helper Method that will build out an environment configuration.
 *
 * If the required configuration is not present, the program will
 * with a code of 1. Printing out the missing environment variable.
 *
 * Type casting is done based on the parameter. The following types
 * are supported.
 *
 * **Sample**
 *
 * ```kotlin
 * data class ServiceEnvConfig (
 *       override val ClusterPort: Int,
 *       val SecretAPIKey: String,
 *       val env: String
 * ) : EnvironmentConfiguration
 *
 * val serviceEnvConfig = loadConfigurationFromEnvironment(ServiceEnvConfig ::class)
 * ```
 *
 * The above will instaniate the environment config. This should be done in the main
 * entry point.
 *
 * @author sean.obrien@bottlerocketstudios.com
 * @since 0.1.0
 * @param clazz The service Environment Configuration record type.
 * @return An instance of the environment configuration.
 */
inline fun <reified C : EnvironmentConfiguration> loadConfigurationFromEnvironment(clazz: KClass<C>): C {
    val cons = clazz.primaryConstructor!!
    val params = cons.parameters
    val values = params
            .map {

                try {
                    it to when (it.type.javaType) {
                        Int::class.java -> System.getenv(it.name).toInt()
                        Long::class.java -> System.getenv(it.name).toLong()
                        Double::class.java -> System.getenv(it.name).toDouble()
                        String::class.java -> System.getenv(it.name).toString()
                        else -> System.getenv(it.name).toString()
                    }
                } catch (e: Exception) {
                    println("Failed to retrieve ${it.name} from environment, exiting. Required configuration is not present")
                    exitProcess(1)
                }
            }
            .toMap()
    return cons.callBy(values)
}