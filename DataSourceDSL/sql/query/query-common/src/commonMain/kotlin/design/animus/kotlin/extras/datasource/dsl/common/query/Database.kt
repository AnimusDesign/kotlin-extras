package design.animus.kotlin.extras.datasource.dsl.common.query

interface Database {
    val database: String
    val schema: String
}