package design.animus.kotlin.extras.datasource.dsl.common.builder

import design.animus.kotlin.extras.datasource.dsl.common.SQLConditionalBuilder
import design.animus.kotlin.extras.datasource.dsl.common.query.*
import design.animus.kotlin.extras.datasource.dsl.common.query.verbs.buildSelectQuery
import design.animus.kotlin.extras.datasource.dsl.common.query.verbs.buildUpdateQuery

class CommonSQLQuery<D : Database, T : ITable<D>> {
    internal lateinit var mutableQuery: MutableQuery<D, T>
    override fun toString() =
            when (mutableQuery.verb) {
                SQLVerbs.SELECT -> buildSelectQuery(mutableQuery as SelectMutableQuery).trim()
                SQLVerbs.UPDATE -> if (mutableQuery is UpdateMutableQuery) {
                    buildUpdateQuery(mutableQuery as UpdateMutableQuery).trim()
                } else throw Exception("Was expecting Update query but got ${mutableQuery::class}")
                SQLVerbs.INSERT -> TODO()
                SQLVerbs.DELETE -> TODO()
            }

    // Common
    // End Common

    //Select
    fun select() = SelectMutableQuery<D, T>(SQLVerbs.SELECT, listOf(), conditions = listOf())

    fun <C : Any> select(vararg fields: Fields<C, D, T>) = SelectMutableQuery<D, T>(SQLVerbs.SELECT, fields.toList(), conditions = listOf())

    infix fun SelectMutableQuery<D, T>.from(inTable: T): SelectMutableQuery<D, T> {
        this.table = Table<D, T>(inTable)
        return this
    }

    infix fun SelectMutableQuery<D, T>.from(inTable: NamedTable<D, T>): SelectMutableQuery<D, T> {
        this.table = inTable
        return this
    }

    suspend infix fun SelectMutableQuery<D, T>.where(block: SQLConditionalBuilder.() -> Unit): SelectMutableQuery<D, T> {
        val inTable = this.table
        val state = SQLConditionalBuilder()
        state.block()
        return this.copy(conditions = state.conditions).apply { table = inTable }
    }
    // End Select

    //Update
    private fun buildInitialQuery(table: QueryTable): UpdateMutableQuery<D, T> {
        val query = UpdateMutableQuery<D, T>(SQLVerbs.UPDATE, listOf())
        query.table = table
        return query
    }

    suspend fun update(table: T) = buildInitialQuery(Table(table))
    suspend fun update(table: NamedTable<D, T>) = buildInitialQuery(table)

    suspend infix fun UpdateMutableQuery<D, T>.set(block: UpdateSet<D, T>.() -> Unit): UpdateMutableQuery<D, T> {
        val state = UpdateSet<D, T>()
        state.block()
        this.changes.addAll(state.changes)
        return this
    }

    suspend infix fun UpdateMutableQuery<D, T>.where(block: SQLConditionalBuilder.() -> Unit): UpdateMutableQuery<D, T> {
        val inTable = this.table
        val state = SQLConditionalBuilder()
        state.block()
        return this.copy(conditions = state.conditions).apply { table = inTable }
    }
    // End Update
}

suspend fun <D : Database, T : ITable<D>> sqlQuery(block: suspend CommonSQLQuery<D, T>.() -> MutableQuery<D, T>): CommonSQLQuery<D, T> {
    val state = CommonSQLQuery<D, T>()
    val generatedQuery = state.block()
    state.apply {
        mutableQuery = generatedQuery
    }
    return state
}