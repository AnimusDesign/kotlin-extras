package design.animus.kotlin.extras.datasource.dsl.common.builder

import design.animus.kotlin.extras.datasource.dsl.common.query.Database
import design.animus.kotlin.extras.datasource.dsl.common.query.Field
import design.animus.kotlin.extras.datasource.dsl.common.query.ITable
import design.animus.kotlin.extras.datasource.dsl.common.query.verbs.UpdateChange


class UpdateSet<D : Database, T : ITable<D>> {
    val changes: MutableList<UpdateChange<*, D, T>> = mutableListOf()

    infix fun <C : Any, F : Field<C, D, T>> F.TO(value: C): UpdateChange<C, D, T> {
        val change = UpdateChange<C, D, T>(this, value)
        changes.add(change)
        return change
    }
}