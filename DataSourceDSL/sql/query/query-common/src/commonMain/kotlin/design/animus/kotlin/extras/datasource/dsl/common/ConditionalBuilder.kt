package design.animus.kotlin.extras.datasource.dsl.common

import design.animus.kotlin.extras.datasource.dsl.common.query.*

class SQLConditionalBuilder {
    val conditions = mutableListOf<ConditionalSteps>()

    private fun <C : Any, D : Database, T : ITable<D>> Field<C, D, T>.conditionBuilder(value: C, operator: SQLConditionalOperator): Condition<*, D, T> {
        val con = Condition<C, D, T>(
                this,
                operator,
                value) as Condition<*, D, T>
        conditions.add(con)
        return con
    }

    infix fun <C : Any, D : Database, T : ITable<D>> Field<C, D, T>.equal(value: C) = conditionBuilder(value, SQLConditionalOperator.Equal)
    infix fun <C : Any, D : Database, T : ITable<D>> Field<C, D, T>.gt(value: C) = conditionBuilder(value, SQLConditionalOperator.GreaterThan)
    infix fun <C : Any, D : Database, T : ITable<D>> Field<C, D, T>.gte(value: C) = conditionBuilder(value, SQLConditionalOperator.GreaterThanOrEqual)
    infix fun <C : Any, D : Database, T : ITable<D>> Field<C, D, T>.lte(value: C) = conditionBuilder(value, SQLConditionalOperator.LessThanOrEqual)
    infix fun <C : Any, D : Database, T : ITable<D>> Field<C, D, T>.like(value: C) = conditionBuilder(value, SQLConditionalOperator.Like)
    infix fun <C : Any, D : Database, T : ITable<D>> Field<C, D, T>.iLike(value: C) = conditionBuilder(value, SQLConditionalOperator.CaseInsensitiveLike)
    infix fun <C : Any, D : Database, T : ITable<D>> Field<C, D, T>.notEqual(value: C) = conditionBuilder(value, SQLConditionalOperator.NotEqual)

    private fun <C : Any, D : Database, T : ITable<D>> handleConditionOperator(value: Condition<C, D, T>, operator: ConditionalSteps): Condition<*, D, T> {
        val index = conditions.size - 1
        val lastCondition = conditions[index]
        conditions.removeAt(index)
        conditions.add(operator)
        conditions.add(lastCondition)
        return value
    }

    infix fun <C : Any, D : Database, T : ITable<D>> Condition<*, D, T>.and(value: Condition<C, D, T>) = handleConditionOperator(value, AND)
    infix fun <C : Any, D : Database, T : ITable<D>> Condition<*, D, T>.or(value: Condition<C, D, T>) = handleConditionOperator(value, OR)
    infix fun <C : Any, D : Database, T : ITable<D>> Condition<*, D, T>.not(value: Condition<C, D, T>) = handleConditionOperator(value, NOT)

}