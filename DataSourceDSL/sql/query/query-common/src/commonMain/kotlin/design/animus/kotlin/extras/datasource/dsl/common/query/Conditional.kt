package design.animus.kotlin.extras.datasource.dsl.common.query

typealias WhereString = String

interface CommonCondition<C : Any, D : Database, T : ITable<D>> {
    val column: Field<C, D, T>
    val operator: SQLConditionalOperator
    val expectedValue: C
}

interface CommonConditionChain

sealed class ConditionalSteps
data class Condition<C : Any, D : Database, T : ITable<D>>(
        override val column: Field<C, D, T>,
        override val operator: SQLConditionalOperator,
        override val expectedValue: C
) : CommonCondition<C, D, T>, ConditionalSteps() {
    override fun toString() =
            "Condition:[${this.column.columnName} ${this.operator.operator} '${this.expectedValue}']"

}

object AND : CommonConditionChain, ConditionalSteps() {
    override fun toString() = "AND"
}

object OR : CommonConditionChain, ConditionalSteps() {
    override fun toString() = "OR"
}

object NOT : CommonConditionChain, ConditionalSteps() {
    override fun toString() = "NOT"
}

/**
 * @author Animus Null
 * @since 1.0
 * @throws IllegalArgumentException On an unsupported query, i.e. one that does not use conditions.
 * @return A string representing the where condition
 */
internal fun <D : Database, T : ITable<D>> buildWhereString(query: MutableQuery<D, T>, table: FriendlyTableName): WhereString = when (query) {
    is SelectMutableQuery<D, T>, is UpdateMutableQuery<D, T> -> {
        when {
            query.conditions.isEmpty() -> ""
            query.conditions.size == 1 -> {
                when (val condition = query.conditions.first()) {
                    is Condition<*, *, *> -> {
                        val value = condition.expectedValue
                        println(value::class)
                        val v = if (value is Int || value is Double || value is Float || value is Long)
                            "$value" else "'$value'"
                        "where $table.${condition.column.columnName} ${condition.operator.operator} $v"
                    }
                    else -> throw IllegalArgumentException("If one condition it must be a Condition and not Operator")
                }

            }
            else -> {
                "where " + query.conditions.mapIndexed { index, condition ->
                    when (condition) {
                        is Condition<*, *, *> -> "$table.${condition.column.columnName} ${condition.operator.operator} '${condition.expectedValue}'"
                        AND -> "AND"
                        OR -> "OR"
                        NOT -> "NOT"
                    }
                }.joinToString(" ")
            }
        }
    }
    else -> throw IllegalArgumentException("This query type [${query::class}] does not support conditions.")
}