package design.animus.kotlin.extras.datasource.dsl.common.query.verbs

import design.animus.kotlin.extras.datasource.dsl.common.query.*

interface ISelectQuery {
    val verb: SQLVerbs
    val columns: List<Fields<*, *, *>>
    val conditions: List<ConditionalSteps>
}

interface ISelectJoinQuery : ISelectQuery

internal fun <D : Database, T : ITable<D>> buildSelectQuery(query: SelectMutableQuery<D, T>): String {
    val (schemaTable, friendlyTable) = getSchemaTable(query)
    val columns = if (query.columns.isEmpty()) "*" else query.columns.joinToString(",") {
        when (it) {
            is PrimaryKey -> "$schemaTable.${it.columnName}"
            is Field -> "$schemaTable.${it.columnName}"
            is NamedField -> "$schemaTable.${it.field.columnName} as ${it.name}"
            is PrimaryKeys -> TODO()
        }
    }
    val where = buildWhereString(query, friendlyTable)
    return """
        ${query.verb.name} $columns from $schemaTable $where
    """.trimIndent()
}