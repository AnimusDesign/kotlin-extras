package design.animus.kotlin.extras.datasource.dsl.common.query

sealed class QueryTable
data class Table<D : Database, T : ITable<D>>(val table: T) : QueryTable()
data class NamedTable<D : Database, T : ITable<D>>(
        val table: T,
        val name: String
) : QueryTable()

interface ITable<D : Database> {
    val name: String
    val database: D
}

infix fun <D : Database, T : ITable<D>> T.AS(value: String) = NamedTable(this, value)
typealias TableNamePair = Pair<SchemaTableName, FriendlyTableName>
typealias SchemaTableName = String
typealias FriendlyTableName = String

/**
 * @return A Pair first being the full name space of the table, second being an alias if named or the full schema.
 */
@Suppress("UNCHECKED_CAST")
internal fun <D : Database, T : ITable<D>> getSchemaTable(query: MutableQuery<D, T>): TableNamePair =
        when (query.table) {
            is Table<*, *> -> {
                val table = (query.table as Table<D, T>).table
                Pair("${table.database.schema}.${table.database.database}.${table.name}",
                        "${table.database.schema}.${table.database.database}.${table.name}")
            }
            is NamedTable<*, *> -> {
                val namedTable = query.table as NamedTable<D, T>
                Pair("${namedTable.table.database.schema}.${namedTable.table.database.database}.${namedTable.table.name} as ${namedTable.name}",
                        namedTable.name
                )
            }
        }