package design.animus.kotlin.extras.datasource.dsl.common.query

enum class SQLVerbs {
    SELECT,
    UPDATE,
    INSERT,
    DELETE
}