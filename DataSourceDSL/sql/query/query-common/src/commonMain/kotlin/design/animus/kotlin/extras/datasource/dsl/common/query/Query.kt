package design.animus.kotlin.extras.datasource.dsl.common.query

import design.animus.kotlin.extras.datasource.dsl.common.query.verbs.ISelectQuery
import design.animus.kotlin.extras.datasource.dsl.common.query.verbs.IUpdateQuery
import design.animus.kotlin.extras.datasource.dsl.common.query.verbs.UpdateChange

sealed class MutableQuery<D : Database, T : ITable<D>>(
        open val verb: SQLVerbs,
        open val conditions: List<ConditionalSteps>
) {
    lateinit var table: QueryTable
}

data class SelectMutableQuery<D : Database, T : ITable<D>>(
        override val verb: SQLVerbs,
        override val columns: List<Fields<*, D, T>>,
        override val conditions: List<ConditionalSteps>
) : ISelectQuery, MutableQuery<D, T>(verb, conditions)

data class UpdateMutableQuery<D : Database, T : ITable<D>>(
        override val verb: SQLVerbs,
        override val conditions: List<ConditionalSteps>,
        override val changes: MutableList<UpdateChange<*, D, T>> = mutableListOf()
) : IUpdateQuery<D, T>, MutableQuery<D, T>(verb, conditions)