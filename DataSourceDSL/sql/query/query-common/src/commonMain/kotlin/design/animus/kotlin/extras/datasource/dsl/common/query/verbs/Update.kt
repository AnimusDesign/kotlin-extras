package design.animus.kotlin.extras.datasource.dsl.common.query.verbs

import design.animus.kotlin.extras.datasource.dsl.common.query.*

typealias UpdateSetString = String

data class UpdateChange<C : Any, D : Database, T : ITable<D>>(
        val field: Field<C, D, T>,
        val value: C
) {
    override fun toString(): String =
            "Set[${this.field.columnName} = ${this.value}]"
}

interface IUpdateQuery<D : Database, T : ITable<D>> {
    val conditions: List<ConditionalSteps>
    val changes: MutableList<UpdateChange<*, D, T>>
}

internal fun <D : Database, T : ITable<D>> buildUpdateQuery(query: UpdateMutableQuery<D, T>): String {
    val (schemaTable, friendlyTable) = getSchemaTable(query)
    val where = buildWhereString(query, friendlyTable)
    val changes = query.changes.joinToString(",") { change ->
        val value = when (change.value) {
            is Int, is Long, is Float, is Double -> "${change.value}"
            else -> "'${change.value}'"
        }
        "$friendlyTable.${change.field.columnName} = $value"
    }
    return """
        update $schemaTable SET $changes $where  
    """.trimIndent()
}