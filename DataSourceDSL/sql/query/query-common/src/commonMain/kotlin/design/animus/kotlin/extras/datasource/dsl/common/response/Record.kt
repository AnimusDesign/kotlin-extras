package design.animus.kotlin.extras.datasource.dsl.common.response

import design.animus.kotlin.extras.datasource.dsl.common.query.Database
import design.animus.kotlin.extras.datasource.dsl.common.query.ITable

interface Record<D:Database, T: ITable<D>>