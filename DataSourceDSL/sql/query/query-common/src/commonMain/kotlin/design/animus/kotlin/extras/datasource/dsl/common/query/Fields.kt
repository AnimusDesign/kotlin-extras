package design.animus.kotlin.extras.datasource.dsl.common.query

data class PrimaryKeyColumn(
        val columnName: String,
        val columnType: String
)

sealed class Fields<C : Any, D : Database, T : ITable<D>>
abstract class Field<C : Any, D : Database, T : ITable<D>>(
        val columnName: String,
        val columnType: String
) : Fields<C, D, T>()

data class NamedField<C : Any, D : Database, T : ITable<D>>(
        val field: Field<C, D, T>,
        val name: String

) : Fields<C, D, T>()

abstract class PrimaryKey<C : Any, D : Database, T : ITable<D>>(
        columnName: String,
        columnType: String
) : Field<C, D, T>(columnName, columnType)

abstract class PrimaryKeys<C : Any, D : Database, T : ITable<D>>(
        val columnName: List<PrimaryKeyColumn>
) : Fields<C, D, T>()

infix fun <C : Any, D : Database, T : ITable<D>> Field<C, D, T>.AS(value: String) = NamedField<C, D, T>(this, value)
