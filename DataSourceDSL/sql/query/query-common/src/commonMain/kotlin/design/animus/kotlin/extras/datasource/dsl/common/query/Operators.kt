package design.animus.kotlin.extras.datasource.dsl.common.query

enum class SQLConditionalOperator(val operator: String) {
    Equal("="),
    NotEqual("!="),
    GreaterThan(">"),
    GreaterThanOrEqual(">="),
    LessThan("<"),
    LessThanOrEqual("<="),
    Like("like"),
    CaseInsensitiveLike("ilike")
}