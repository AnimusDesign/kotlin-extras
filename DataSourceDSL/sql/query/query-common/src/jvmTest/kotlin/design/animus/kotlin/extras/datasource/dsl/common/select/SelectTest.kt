package design.animus.kotlin.extras.datasource.dsl.common.select

import design.animus.kotlin.extras.datasource.dsl.common.builder.sqlQuery
import design.animus.kotlin.extras.datasource.dsl.common.query.AS
import design.animus.kotlin.extras.datasource.dsl.common.schema.ExampleDatabase
import design.animus.kotlin.extras.datasource.dsl.common.schema.Users
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertTrue

class SelectTest {
    @Test
    fun testQuerySelectAll() = runBlocking {
        val query = sqlQuery<ExampleDatabase, Users> { select() from Users }.toString()
        assertTrue { query == "SELECT * from public.example.users" }
    }

    @Test
    fun testQuerySelectOneField() = runBlocking {
        val query = sqlQuery<ExampleDatabase, Users> { select(Users.firstName) from Users }.toString()
        assertTrue { query == "SELECT public.example.users.first_name from public.example.users" }
    }

    @Test
    fun testQuerySelectOneNamedField() = runBlocking {
        val query = sqlQuery<ExampleDatabase, Users> { select(Users.firstName AS "first_name") from Users }.toString()
        assertTrue { query == "SELECT public.example.users.first_name as first_name from public.example.users" }
    }

    @Test
    fun testQuerySelectTwoFields() = runBlocking {
        val query = sqlQuery<ExampleDatabase, Users> { select(Users.firstName, Users.lastName) from Users }.toString()
        println(query.toString())
        assertTrue { query == "SELECT public.example.users.first_name,public.example.users.last_name from public.example.users" }
    }

    @Test
    fun testQueryWithOneCondition() {
        runBlocking {
            val query = sqlQuery<ExampleDatabase, Users> {
                val query = select() from Users where {
                    (Users.id equal 1)
                }
                query
            }
            assertTrue {
                (query.toString()) ==
                        "SELECT * from public.example.users where public.example.users.id = 1"
            }
        }
    }

    @Test
    fun testQueryWithTwoConditions() {
        runBlocking {
            val query = sqlQuery<ExampleDatabase, Users> {
                select() from Users where {
                    (Users.id equal 1) and (Users.firstName equal "John")
                }
            }
            assertTrue {
                (query.toString()) ==
                        "SELECT * from public.example.users where public.example.users.id = '1' AND public.example.users.first_name = 'John'"
            }
        }
    }

    @Test
    fun testQueryWithThreeConditions() {
        runBlocking {
            val query = sqlQuery<ExampleDatabase, Users> {
                select() from Users where {
                    (Users.id equal 1) and (Users.firstName equal "John") or (Users.lastName equal "Doe")
                }
            }
            assertTrue {
                (query.toString()) ==
                        "SELECT * from public.example.users where public.example.users.id = '1' AND public.example.users.first_name = 'John' OR public.example.users.last_name = 'Doe'"
            }
        }
    }

    @Test
    fun testQueryAllWithNamedTable() = runBlocking {

        val query = sqlQuery<ExampleDatabase, Users> {
            select() from (Users AS "users_table")
        }
        println(query)
        assertTrue {
            query.toString() ==
                    "SELECT * from public.example.users as users_table"
        }
    }

    @Test
    fun testQueryAllWithNamedTableAndOneCondition() = runBlocking {
        val query = sqlQuery<ExampleDatabase, Users> {
            select() from (Users AS "users_table") where {
                Users.id equal 1
            }
        }
        println(query.toString())
        assertTrue {
            query.toString() ==
                    "SELECT * from public.example.users as users_table where users_table.id = 1"
        }
    }
}