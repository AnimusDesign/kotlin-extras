package design.animus.kotlin.extras.datasource.dsl.common.schema

import design.animus.kotlin.extras.datasource.dsl.common.query.Field
import design.animus.kotlin.extras.datasource.dsl.common.query.ITable

object Profile : ITable<ExampleDatabase> {
    override val database = ExampleDatabase
    override val name = "user_profile"
    val id: Field<Long, ExampleDatabase, Profile> = object : Field<Long, ExampleDatabase, Profile>("id", "bigint") {}
}