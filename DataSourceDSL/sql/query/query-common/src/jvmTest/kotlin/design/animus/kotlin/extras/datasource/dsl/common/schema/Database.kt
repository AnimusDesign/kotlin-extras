package design.animus.kotlin.extras.datasource.dsl.common.schema

import design.animus.kotlin.extras.datasource.dsl.common.query.Database

object ExampleDatabase : Database {
    override val database = "example"
    override val schema = "public"
}