package design.animus.kotlin.extras.datasource.dsl.common.schema

import design.animus.kotlin.extras.datasource.dsl.common.query.Field
import design.animus.kotlin.extras.datasource.dsl.common.query.ITable
import design.animus.kotlin.extras.datasource.dsl.common.query.PrimaryKey

object Users : ITable<ExampleDatabase> {
    val id: PrimaryKey<Long, ExampleDatabase, Users> = object : PrimaryKey<Long, ExampleDatabase, Users>("id", "bigint") {}
    val firstName: Field<String, ExampleDatabase, Users> = object : Field<String, ExampleDatabase, Users>("first_name", "varchar(24)") {}
    val lastName: Field<String, ExampleDatabase, Users> = object : Field<String, ExampleDatabase, Users>("last_name", "varchar(24)") {}
    override val name = "users"
    override val database = ExampleDatabase
}