package design.animus.kotlin.extras.datasource.dsl.common.select

import design.animus.kotlin.extras.datasource.dsl.common.builder.sqlQuery
import design.animus.kotlin.extras.datasource.dsl.common.schema.ExampleDatabase
import design.animus.kotlin.extras.datasource.dsl.common.schema.Profile
import design.animus.kotlin.extras.datasource.dsl.common.schema.Users
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertTrue

class UpdateTest {
    @Test
    fun updateOneField() = runBlocking {
        val query = sqlQuery<ExampleDatabase, Users> {
            update(Users) set {
                Users.firstName TO "Bob"
                Users.lastName TO "Smith"
            } where {
                (Users.id equal 1)
            }
        }
        assertTrue {
            query.toString() ==
                    "update public.example.users SET public.example.users.first_name = 'Bob',public.example.users.last_name = 'Smith' where public.example.users.id = 1"
        }

    }
}