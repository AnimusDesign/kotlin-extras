package design.animus.datasource.sql.schema.postgresql

import design.animus.datasource.sql.schema.common.SchemaGeneratorConfig
import design.animus.datasource.sql.schema.postgresql.generator.generateDatabaseCode
import design.animus.datasource.sql.schema.postgresql.generator.generateSQLTableCode
import design.animus.datasource.sql.schema.postgresql.utils.getSchema
import design.animus.datasource.sql.schema.postgresql.utils.groupByTable
import kotlinx.coroutines.runBlocking
import org.gradle.api.Plugin
import org.gradle.api.Project

open class PostgresSQLConfiguration : SchemaGeneratorConfig() {
    override var dataBases: List<String> = listOf()
    override var dataBaseHost: String = "localhost"
    override var dataBaseUser: String = "postgres"
    override var dataBasePassword: String = "postgres"
    override var dataBasePort: Int = 5432
    override var namespace: String = "design.animus.datasource.sql.generated"
    override var outputDir: String = "./"
    var dataBaseSchema: String = "public"
}

class PostgresSQLSchemaGenerator : Plugin<Project> {

    override fun apply(project: Project) {
        val extension = project.extensions.create<PostgresSQLConfiguration>("greeting", PostgresSQLConfiguration::class.java)
        project.task("checkConnection")
                .doLast { _ ->
                    extension.dataBases.forEach { database ->
                        val connection = getPostgresConnection(database, extension)
                        println("Database: $database => Connected => ${connection.isConnected()}")
                        connection.disconnect()
                    }
                }
        project.task("buildSchema")
                .doLast { _ ->

                    extension.dataBases.forEach { db ->
                        runBlocking {
                            val conn = getPostgresConnection(db, extension)
                            val schemaRows = getSchema(extension.dataBaseSchema, db, conn)
                            val tables = groupByTable(schemaRows, extension)
                            val databaseClass = generateDatabaseCode(db, extension)
                            tables.forEach { (inTable, columns) ->
                                generateSQLTableCode(databaseClass, db, inTable, columns, extension)
                            }
                            conn.disconnect()
                        }
                    }

                }
    }
}