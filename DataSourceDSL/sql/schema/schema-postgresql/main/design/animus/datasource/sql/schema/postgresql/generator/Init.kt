package design.animus.datasource.sql.schema.postgresql.generator

import com.squareup.kotlinpoet.*
import design.animus.datasource.sql.schema.postgresql.PostgresSQLConfiguration
import design.animus.datasource.sql.schema.postgresql.RawInformationSchema
import design.animus.datasource.sql.schema.postgresql.renameClass
import design.animus.kotlin.extras.datasource.dsl.common.query.Database
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

internal suspend fun generateDatabaseCode(db: String, config: PostgresSQLConfiguration): ClassName {
    val packageName = "${config.namespace}.database.$db"
    println(packageName)
    val databaseClassName = renameClass(db)
    println(databaseClassName)
    val file = FileSpec.builder(packageName, databaseClassName)
    file.addType(
            TypeSpec.Companion.objectBuilder(databaseClassName)
                    .addSuperinterface(Database::class)
                    .addProperty(
                            PropertySpec.builder("database", String::class)
                                    .initializer("%S", db)
                                    .addModifiers(KModifier.OVERRIDE)
                                    .build()
                    )
                    .addProperty(
                            PropertySpec.builder("schema", String::class)
                                    .initializer("%S", config.dataBaseSchema)
                                    .addModifiers(KModifier.OVERRIDE)
                                    .build()
                    )
                    .build()
    )
    file.build().writeTo(File(config.outputDir))
    return ClassName(packageName, databaseClassName)
}

/**
 * @author Animus Null
 * @since 0.1.0
 * @param db The database the table lives in
 * @param inTable A single Information Schema which will be used to generate the table information.
 * @param columns A list of information schema which will be used to generate the fields.
 * @param config An instance of the Plugin configuration
 */
internal suspend fun generateSQLTableCode(databaseClass: ClassName,
                                          db: String,
                                          inTable: RawInformationSchema,
                                          columns: List<RawInformationSchema>,
                                          config: PostgresSQLConfiguration) {
    val tableClassName = renameClass(inTable.tableName)
    val packageName = "${config.namespace}.database.$db.tables"
    val file = FileSpec.builder(packageName, tableClassName)
    val customTableInterface = ClassName(packageName, tableClassName)
    val tableTypeSpec = buildTableObject(databaseClass, tableClassName, customTableInterface, inTable.tableName, inTable.tableSchema)
    val columnTypes = buildTableColumns(databaseClass, customTableInterface, columns)
    columnTypes.forEach { (_, _, tableProp) ->
        tableTypeSpec.addProperty(tableProp)
    }
    val recordTypeSpec = buildTableRecord(databaseClass, tableClassName, customTableInterface, columnTypes.map { it.first }, columnTypes.map { it.second })
    file.addType(tableTypeSpec.build())
    file.addType(recordTypeSpec)
    val outputDir = Paths.get(File(config.outputDir).absolutePath, packageName.replace(".", "/")).toFile()
//    val codeFile = Paths.get(outputDir.absolutePath, "$tableClassName.kt")
//    println(codeFile)
    if (!outputDir.exists()) Files.createDirectories(outputDir.toPath())
    file.build().writeTo(File(config.outputDir))
}