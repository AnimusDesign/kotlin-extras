package design.animus.datasource.sql.schema.postgresql

import com.github.jasync.sql.db.postgresql.PostgreSQLConnectionBuilder
import design.animus.datasource.sql.schema.common.SchemaGeneratorConfig


fun renameProperty(prop: String): String {
    val phrases = prop.split("_")
    val rsp = mutableListOf<String>()
    rsp.add(phrases.first().decapitalize())
    rsp.addAll(
            phrases.subList(1, phrases.size).map {
                it.capitalize()
            }
    )
    return rsp.joinToString("")
}

fun renameClass(clazz: String): String = clazz.split("_").joinToString("") {
    it.capitalize()
}

internal fun getPostgresConnection(database: String, config: SchemaGeneratorConfig) = PostgreSQLConnectionBuilder.createConnectionPool(
        "jdbc:postgresql://${config.dataBaseHost}:${config.dataBasePort}/${database}?user=${config.dataBaseUser}&password=${config.dataBasePassword}")