package design.animus.datasource.sql.schema.postgresql

internal data class RawInformationSchema(val map: Map<String, Any?>) {
    val tableCatalog: String by map
    val tableSchema: String by map
    val tableName: String by map
    val columnName: String by map
    val ordinalPosition: Int by map
    val columnDefault: Any? by map
    val isNullable: String by map
    val dataType: String? by map
    val characterMaximumLength: Int? by map
    val characterOctetLength: Int? by map
    val numericPrecision: Int? by map
    val numericPrecisionRadix: Int? by map
    val numericScale: Int? by map
    val datetimePrecision: Int? by map
    val intervalType: Int? by map
    val intervalPrecision: String? by map
    val characterSetCatalog: String? by map
    val characterSetSchema: String? by map
    val characterSetName: String? by map
    val collationCatalog: String? by map
    val collationSchema: String? by map
    val collationName: String? by map
    val domainCatalog: String? by map
    val domainSchema: String? by map
    val domainName: String? by map
    val udtCatalogString by map
    val udtSchema: String by map
    val udtName: String by map
    val scopeCatalog: String? by map
    val scopeSchema: String? by map
    val scopeName: String? by map
    val maximumCardinality: String? by map
    val dtdIdentifier: String by map
    val isSelfReferencing: String by map
    val isIdentityString by map
    val identityGeneration: String? by map
    val identityStart: String? by map
    val identityIncrement: String? by map
    val identityMaximum: String? by map
    val identityMinimum: String? by map
    val identityCycle: String by map
    val isGenerated: String by map
    val generationExpression: String? by map
    val isUpdatable: String by map
}

internal val informationSchemaColumns = listOf(
        "tableCatalog",
        "tableSchema",
        "tableName",
        "columnName",
        "ordinalPosition",
        "columnDefault",
        "isNullable",
        "dataType",
        "characterMaximumLength",
        "characterOctetLength",
        "numericPrecision",
        "numericPrecisionRadix",
        "numericScale",
        "datetimePrecision",
        "intervalType",
        "intervalPrecision",
        "characterSetCatalog",
        "characterSetSchema",
        "characterSetName",
        "collationCatalog",
        "collationSchema",
        "collationName",
        "domainCatalog",
        "domainSchema",
        "domainName",
        "udtCatalog",
        "udtSchema",
        "udtName",
        "scopeCatalog",
        "scopeSchema",
        "scopeName",
        "maximumCardinality",
        "dtdIdentifier",
        "isSelfReferencing",
        "isIdentity",
        "identityGeneration",
        "identityStart",
        "identityIncrement",
        "identityMaximum",
        "identityMinimum",
        "identityCycle",
        "isGenerated",
        "generationExpression",
        "isUpdatable"
)