package design.animus.datasource.sql.schema.postgresql.generator

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.PropertySpec
import design.animus.datasource.sql.schema.postgresql.RawInformationSchema
import design.animus.datasource.sql.schema.postgresql.renameClass
import design.animus.datasource.sql.schema.postgresql.renameProperty
import design.animus.datasource.sql.schema.postgresql.utils.getColumnClassType
import design.animus.kotlin.extras.datasource.dsl.common.query.Field

internal suspend fun buildTableColumns(databaseClass: ClassName, customTableInterface: ClassName, columns: List<RawInformationSchema>) = columns.map { col ->
    val fieldClassName = renameClass(col.columnName)
    val fieldProp = renameProperty(col.columnName)
    val fieldType = getColumnClassType(col.dataType!!)
    val fieldClass = ClassName(
            Field::class.qualifiedName!!.replace(".Field", ""),
            Field::class.simpleName!!)
            .parameterizedBy(
                    ClassName(fieldType.qualifiedName!!.replace(".${fieldType.simpleName}", ""), fieldType.simpleName!!),
                    databaseClass,
                    customTableInterface
            )
    Triple(

            ParameterSpec
                    .builder(fieldProp, fieldType)
                    .build(),
            PropertySpec
                    .builder(fieldProp, fieldType)
                    .initializer(fieldProp)
                    .build(),
            PropertySpec.builder(fieldProp, fieldClass)
                    .initializer("%L",
                            "object : Field<${fieldType.simpleName}, ${databaseClass.simpleName},  ${customTableInterface.simpleName}>(\"${col.columnName}\", \"\") {}"
                    )
                    .build()
    )
}