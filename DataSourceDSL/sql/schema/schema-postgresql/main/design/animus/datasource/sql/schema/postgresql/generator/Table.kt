package design.animus.datasource.sql.schema.postgresql.generator

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeSpec
import design.animus.kotlin.extras.datasource.dsl.common.query.ITable

suspend fun buildTableInterface(tableClassName: String, rawTableName: String, rawTableSchema: String) = TypeSpec
        .interfaceBuilder("I$tableClassName")
        .addSuperinterface(ITable::class)
        .build()

/**
 * @author Animus Null
 */
suspend fun buildTableObject(databaseClass: ClassName, tableClassName: String, customTableInterface: ClassName, rawTableName: String, rawTableSchema: String) = TypeSpec.objectBuilder(tableClassName)
        .addSuperinterface(
                ClassName(
                        ITable::class.qualifiedName!!.replace(".ITable", ""),
                        ITable::class.simpleName!!)
                        .parameterizedBy(databaseClass)
        )
        .addProperty(
                PropertySpec.builder("name", String::class)
                        .initializer("%S", rawTableName)
                        .addModifiers(KModifier.OVERRIDE)
                        .build()
        )
        .addProperty(
                PropertySpec.builder("database", databaseClass)
                        .initializer("%L", databaseClass.simpleName)
                        .addModifiers(KModifier.OVERRIDE)
                        .build()
        )