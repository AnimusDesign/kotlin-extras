package design.animus.datasource.sql.schema.postgresql.utils

import kotlin.reflect.KClass

enum class SQLTypes(val friendly: String) {
    ARRAY("ARRAY"),
    BigInt("bigint"),
    Boolean("boolean"),
    VarChar("character varying"),
    Date("date"),
    Integer("integer"),
    Numeric("numeric"),
    Text("text"),
    TimeStamp("timestamp without time zone")
}
internal fun getSQLTypeByName(name:String) = SQLTypes.values().filter { it.friendly == name }.firstOrNull()
