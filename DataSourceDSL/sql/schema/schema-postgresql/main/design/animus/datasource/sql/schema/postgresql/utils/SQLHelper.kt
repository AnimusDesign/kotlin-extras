package design.animus.datasource.sql.schema.postgresql.utils

import com.github.jasync.sql.db.pool.ConnectionPool
import com.github.jasync.sql.db.postgresql.PostgreSQLConnection
import design.animus.datasource.sql.schema.postgresql.PostgresSQLConfiguration
import design.animus.datasource.sql.schema.postgresql.RawInformationSchema
import design.animus.datasource.sql.schema.postgresql.informationSchemaColumns
import java.sql.Date
import java.time.OffsetDateTime

/**
 * Get the information schema for a given schema and database
 *
 * @author Animus Null
 * @since 0.1.0
 * @param schema The schema to scan
 * @param database The database to san
 * @param connection An open connection.
 */
internal suspend fun getSchema(schema: String, database: String, connection: ConnectionPool<PostgreSQLConnection>): List<RawInformationSchema> {
    val query = """
                SELECT * from
                information_schema.COLUMNS where table_schema = '$schema' and table_catalog = '$database'
        """.trimIndent()
    val result = connection.sendQuery(query).get()
    return result.rows.map { r ->
        RawInformationSchema((informationSchemaColumns zip r.toList()).toMap())
    }
}

/**
 * @author Animus Null
 * @since 0.1.0
 * @param schemaRows A list of the reflected schema information.
 * @param config The configuration of the Postgres SQL configuration builder.
 * @returns True on success, false on error
 */
internal suspend fun groupByTable(schemaRows: List<RawInformationSchema>, config: PostgresSQLConfiguration) = schemaRows
        .groupBy { it.tableSchema }
        .map { (_, items) ->
            items.groupBy { it.tableName }.map { (_, columns) ->
                Pair(columns.first(), columns)
            }
        }.flatten()

internal suspend fun getColumnClassType(dataType: String) = when (val type = getSQLTypeByName(dataType)) {
    SQLTypes.ARRAY -> Array<String>::class
    SQLTypes.BigInt -> Long::class
    SQLTypes.Boolean -> Boolean::class
    SQLTypes.VarChar -> String::class
    SQLTypes.Date -> Date::class
    SQLTypes.Integer -> Int::class
    SQLTypes.Numeric -> Number::class
    SQLTypes.Text -> String::class
    SQLTypes.TimeStamp -> OffsetDateTime::class
    else -> String::class
}