package design.animus.datasource.sql.schema.postgresql.generator

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import design.animus.kotlin.extras.datasource.dsl.common.response.Record

internal suspend fun buildTableRecord(databaseClassName: ClassName, tableClassName: String, customTableInterface: ClassName, params: List<ParameterSpec>, props: List<PropertySpec>) = TypeSpec
        .classBuilder("${tableClassName}Record")
        .addSuperinterface(
                ClassName(Record::class.qualifiedName!!.replace(".Record", ""), "Record")
                        .parameterizedBy(databaseClassName, customTableInterface)

        )
        .addModifiers(KModifier.DATA)
        .primaryConstructor(
                FunSpec.constructorBuilder()
                        .addParameters(params)
                        .build()
        )
        .addProperties(props)
        .build()