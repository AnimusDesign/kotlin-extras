package design.animus.datasource.sql.schema.common

/**
 * @author Animus Null
 * @since 0.1.0
 * @property dataBases A list of databases to generator schemas of.
 * @property dataBaseHost The database host to connect too.
 * @property dataBasePort The data base port.
 * @property dataBaseUser The user to authenticate with
 * @property dataBasePassword The passsword of the authenticated user
 * @property outputDir Where the generated codes will be stored
 * @property namespace The package of the generated classes.
 */
abstract class SchemaGeneratorConfig {
    abstract var dataBases: List<String>
    abstract var dataBaseHost: String
    abstract var dataBaseUser: String
    abstract var dataBasePassword: String
    abstract var dataBasePort: Int
    abstract var outputDir: String
    abstract var namespace: String
}