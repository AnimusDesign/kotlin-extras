package design.animus.functional.datatypes.either

@Suppress("UNCHECKED_CAST")
sealed class Either<out E, out R> {
    fun isRight() = this is Right
    fun isLeft() = this is Left

    fun forceRight(): R {
        val r = this as Right
        return r.right
    }

    fun forceLeft(): E {
        val l = this as Left
        return l.left
    }

    data class Left<E : Any, R : Any>(val left: E) : Either<E, R>()
    data class Right<E : Any, R : Any>(val right: R) : Either<E, R>()
    companion object {
        fun <E : Any, R : Any> left(left: E): Either<E, R> = Left(left)
        fun <R : Any> right(right: R): Either<Nothing, R> = Right(right)
        fun <E : Any, R : Any> comprehension(block: EitherComprehension<E, R>.() -> R): Either<E, R> {
            val state = EitherComprehension<E, R>()
            return try {
                Either.right(state.block())
            } catch (e: Exception) {
                val error = state.error as E
                left(error)
            }
        }

        suspend fun <E : Any, R : Any> comprehensionAsync(block: suspend EitherComprehensionAsync<E, R>.() -> R): Either<E, R> {
            val state = EitherComprehensionAsync<E, R>()
            return try {
                Either.right(state.block())
            } catch (e: Exception) {
                val error = state.error as E
                left(error)
            }
        }
    }
}