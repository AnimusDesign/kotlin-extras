package design.animus.functional.datatypes.option

import design.animus.functional.datatypes.either.Either
import design.animus.functional.datatypes.interfaces.IComprehension

abstract class AOptionComprehension<S:Any> : IComprehension<Boolean>{
    override var errorOccurred: Boolean = false
    override var error : Boolean? = false

    operator fun <I:Any> Option<I>.component1() = when (this) {
        is Option.None -> {
            errorOccurred = true
            error = true
            throw Exception()
        }
        is Option.Some -> this.item
    }
}

class OptionComprehension<S:Any> : AOptionComprehension<S>()
