package design.animus.functional.datatypes.interfaces

import design.animus.functional.datatypes.either.Either
import design.animus.functional.datatypes.option.Option

interface IComprehension<E:Any> {
    var errorOccurred: Boolean
    var error: E?
}