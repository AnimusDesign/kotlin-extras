package either

import design.animus.functional.datatypes.either.Either
import design.animus.functional.either.datatypes.BaseError
import design.animus.functional.either.datatypes.Error
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

suspend fun <R : Any> CoroutineScope.catchEitherAsync(ctx: CoroutineContext = Dispatchers.Default,
                                                      block: suspend () -> R): Deferred<Either<Error, R>> = coroutineScope {
    async(ctx) {
        try {
            Either.right(block())
        } catch (e: Exception) {
            Either.left<Error, R>(BaseError(cause = e, message = e.message ?: ""))
        }
    }
}

fun <R : Any> Either.Companion.catchEither(block: () -> R) = try {
    Either.right(block())
} catch (e: Exception) {
    Either.left<Error, R>(BaseError(cause = e, message = e.message ?: ""))
}