package design.animus.functional.datatypes.option

sealed class Option<out S:Any> {
    fun isSome() = this is Some
    fun isNone() = this is None

    fun forceSome() : S {
        val some = this as Some
        return some.item
    }

    fun forceNone() : None<Any> {
        return None<Any>()
    }

    class None<I:Any>() : Option<I>()
    data class Some<I:Any>(val item:I) :Option<I>()
    companion object {
        fun <I : Any> comprehension(block: OptionComprehension<I>.() -> I): Option<I> {
            val state = OptionComprehension<I>()
            return try {
                Some(state.block())
            } catch (e: Exception) {
                None()
            }
        }
    }
}