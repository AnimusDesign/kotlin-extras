package design.animus.functional.either.datatypes

interface Error

data class BaseError(
        val cause:Throwable,
        val message:String,
        val code:Int=0
) : Error