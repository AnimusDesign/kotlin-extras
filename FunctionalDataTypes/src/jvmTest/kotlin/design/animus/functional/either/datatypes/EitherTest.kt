package design.animus.functional.either.datatypes

import design.animus.functional.datatypes.either.Either
import either.catchEitherAsync
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertTrue


class JVMEitherTest {
    @Test
    fun testHappyComprehension() {
        val a = Either.right(1)
        val b = Either.right(2)
        val c = Either.right(3)
        val d = Either.right(4)
        val rsp = Either.comprehension<Throwable, Int> {
            val (possibleA) = a
            val (possibleB) = b
            val (possibleC) = c
            val (possibleD) = d
            possibleA + possibleB + possibleC + possibleD
        }
        assertTrue(rsp.isRight())
        assertTrue(
                rsp.forceRight() == 10
        )
    }

    @Test
    fun testUnhappyComprehension() {
        val e: Either<String, Int> = Either.left("bad")
        val rsp = Either.comprehension<String, Int> {
            val (error) = e
            1
        }
        assertTrue(rsp.isLeft())
        assertTrue(
                rsp.forceLeft() == "bad"
        )
    }

    @Test
    fun testAsyncHappyComprehension() = runBlocking {
        val rsp = Either.comprehensionAsync<String, Int> {
            val a = GlobalScope.async { Either.right(1) }
            val b = GlobalScope.async {
                delay(500)
                Either.right(2)
            }
            val (possibleA) = !a
            val (possibleB) = !b
            possibleA + possibleB
        }
        assertTrue { rsp.isRight() }
        assertTrue { rsp.forceRight() == 3 }
    }

    @Test
    fun testAsyncUnHappyComprehension() = runBlocking {
        val rsp = Either.comprehensionAsync<String, Int> {
            val a = GlobalScope.async { Either.right(1) }
            val b = GlobalScope.async {
                delay(500)
                Either.right(2)
            }
            val c = GlobalScope.async {
                delay(500)
                Either.left<String, Int>("Failure")
            }
            val futureD = GlobalScope.async {
                Either.right<Int>(4)
            }
            val d = (futureD)
            val (possibleA) = !a
            val (possibleB) = !b
            val (possibleC) = !c
            possibleA + possibleB + 1
        }
        assertTrue { rsp.isLeft() }
        assertTrue { rsp.forceLeft() == "Failure" }
    }

    @Test
    fun unhappyComprehensionWithCatchAsync() = runBlocking {
        val rsp = Either.comprehensionAsync<Error, String> {
            val futureError = catchEitherAsync<String> { throw Exception("Bad") }
            val (error) = !futureError
            "Should not get here."
        }
        assertTrue { rsp.isLeft() }
        assertTrue {
            val error = rsp.forceLeft()
            error is BaseError
            val baseError = error as BaseError
            baseError.message == "Bad"
        }
    }
}