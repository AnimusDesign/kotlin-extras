package design.animus.functional.either.datatypes

import design.animus.functional.datatypes.either.startsWithCapital
import design.animus.functional.datatypes.option.Option
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class OptionTest {
    @Test
    fun happyComprehension() {
        val a = Option.Some(1)
        val b = Option.Some(2)
        val rsp = Option.comprehension<Int> {
            val (possibleA) = a
            val (possibleB) = b
            possibleA + possibleB
        }
        assertTrue { rsp.isSome() }
        assertTrue { rsp.forceSome() == 3 }
    }

    @Test
    fun unHappyComprehension() {
        val a = Option.Some(1)
        val b = Option.Some(2)
        val c = Option.None<Int>()
        val rsp = Option.comprehension<Int> {
            val (possibleA) = a
            val (possibleB) = b
            val (possibleC) = c
            possibleA + possibleB
        }
        assertTrue { rsp.isNone() }
    }
}

class StringExtensionTest {
    @Test
    fun testSuccess() {
        assertTrue { "Abc".startsWithCapital() }
    }

    @Test
    fun testFailure() {
        assertFalse { "abc".startsWithCapital() }
    }
}