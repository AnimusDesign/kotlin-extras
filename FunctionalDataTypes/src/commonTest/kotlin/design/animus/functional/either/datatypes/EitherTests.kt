package design.animus.functional.either.datatypes

import design.animus.functional.datatypes.either.Either
import design.animus.functional.datatypes.option.Option
import either.catchEither
import kotlin.test.Test
import kotlin.test.assertTrue

class EitherTest {
    @Test
    fun testHappyComprehension() {
        val a = Either.right(1)
        val b = Either.right(2)
        val c = Either.right(3)
        val d = Either.right(4)
        val rsp = Either.comprehension<Throwable, Int> {
            val (possibleA) = a
            val (possibleB) = b
            val (possibleC) = c
            val (possibleD) = d
            possibleA + possibleB + possibleC + possibleD
        }
        assertTrue(rsp.isRight())
        assertTrue(
                rsp.forceRight() == 10
        )
    }

    @Test
    fun testUnhappyComprehension() {
        val e: Either<String, Int> = Either.left("bad")
        val rsp = Either.comprehension<String, Int> {
            val (error) = e
            1
        }
        println(rsp)
        assertTrue(rsp.isLeft())
        assertTrue(
                rsp.forceLeft() == "bad"
        )
    }

    @Test
    fun unhappyComprehensionWithCatch() {
        val rsp = Either.comprehension<Error,String> {
            val (error) = Either.catchEither<String> { throw Exception("Bad") }
            "Should not get Here"
        }
        assertTrue { rsp.isLeft() }
        assertTrue {
            val error = rsp.forceLeft()
            error is BaseError
            val baseError = error as BaseError
            baseError.message == "Bad"
        }
    }
}
