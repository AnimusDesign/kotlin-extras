package design.animus.base.vertx.traceid

import io.vertx.ext.web.Route
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.kotlin.core.http.listenAwait
import java.util.*

private const val TraceIdKey = "traceId"
private const val TraceIdHeader = "TraceId"

/**
 * A wrapper around the route handler. Adding a trace id to the data context.
 *
 * The `getTraceId` method noted below. Is the recommended option. As you will
 * need to retrieve the `TraceId` any way.
 *
 * @see getTraceId
 * @author animus null
 * @param block The lambda that executes the routing block.
 * @since 0.1.0
 */
fun Route.handlerWithTraceId(block: (RoutingContext) -> Unit): Route = this.handler { ctx ->
    val traceId = UUID.randomUUID()
    val data = ctx.data()

    data.putIfAbsent("TraceId", traceId)
    val headers = ctx.response().headers()
    headers.add("TraceId", traceId.toString())
    block(ctx)
}

suspend fun main() {
    val vertx = io.vertx.core.Vertx.vertx()
    val server = vertx.createHttpServer()
    val router = Router.router(vertx)
    router.get("/handleWithTraceId").handlerWithTraceId { ctx ->
        ctx.response().end("handleWithTraceId")
    }
    router.get("/handle").handler { ctx ->
        val traceId = ctx.getTraceId()
        ctx.response().end(traceId.toString())
    }
    server.requestHandler(router)
    server.listenAwait(port = 8080)
}

/**
 * Retrieves the TraceId from the routing context. If not present it is created.
 *
 *
 * When creating, if not present. It will also set the corresponding response Header.
 *
 * ### Sample
 * ```kotlin
 * router.get("/handle").handler { ctx ->
 *   val traceId = ctx.getTraceId()
 *   ctx.response().end(traceId.toString())
 * }
 * ```
 *
 * @author Animus Null
 * @since 0.1.0
 * @return A UUID representing the trace ID.
 */
fun RoutingContext.getTraceId(): UUID {
    val data = this.data()
    return if (data.containsKey(TraceIdKey)) {
        data[TraceIdKey] as UUID
    } else {
        val traceId = UUID.randomUUID()
        this.response().headers().add(TraceIdHeader, traceId.toString())
        data.putIfAbsent(TraceIdKey, traceId)
        data[TraceIdKey] as UUID
    }
}