package design.animus.base.logging.traceid

import mu.KLogger
import java.util.*

/**
 * Helper method to amend *traceId* and *microService* to the log message if present.
 *
 * @author sean.obrien@bottlerocketstudios.com
 * @since 0.1.0
 * @param microServices The service calling the log
 * @param traceId A trace ID for the inbound request
 * @param msg A lazily evaluated message.
 * @return A string
 */
private fun buildLogMessage(traceId: UUID?, msg: () -> Any?): String {
    val traceIdMsg = if (traceId != null) "[TraceId:$traceId]" else ""
    return try {
        "$traceIdMsg${msg().toString()}"
    } catch (e: Exception) {
        "Log message invocation failed: $e"
    }
}

/**
 * A debug log helper that includes the calling *microService* and a *traceId*
 * @author sean.obrien@bottlerocketstudios.com
 * @since 0.1.0
 * @param microServices The service calling the log
 * @param traceId A trace ID for the inbound request
 * @param msg A lazily evaluated message.
 */
fun KLogger.debug(traceId: UUID, msg: () -> Any?) {
    if (isDebugEnabled) debug(buildLogMessage(traceId, msg))
}

/**
 * A warn log helper that includes the calling *microService* and a *traceId*
 * @author sean.obrien@bottlerocketstudios.com
 * @since 0.1.0
 * @param microServices The service calling the log
 * @param traceId A trace ID for the inbound request
 * @param msg A lazily evaluated message.
 */
fun KLogger.warn(traceId: UUID, msg: () -> Any?) {
    if (isWarnEnabled) warn(buildLogMessage(traceId, msg))
}

/**
 * A error log helper that includes the calling *microService* and a *traceId*
 * @author sean.obrien@bottlerocketstudios.com
 * @since 0.1.0
 * @param microServices The service calling the log
 * @param traceId A trace ID for the inbound request
 * @param msg A lazily evaluated message.
 * @param t The exception that caused the error
 */
fun KLogger.error(traceId: UUID, t: Throwable, msg: () -> Any?) {
    if (isErrorEnabled) error(buildLogMessage(traceId, msg), t)
}

/**
 * A error log helper that includes the calling *microService* and a *traceId*
 * @author sean.obrien@bottlerocketstudios.com
 * @since 0.1.0
 * @param microServices The service calling the log
 * @param traceId A trace ID for the inbound request
 * @param msg A lazily evaluated message.
 */
fun KLogger.error(traceId: UUID, msg: () -> Any?) {
    if (isErrorEnabled) error(buildLogMessage(traceId, msg))
}

/**
 * A trace log helper that includes the calling *microService* and a *traceId*
 * @author sean.obrien@bottlerocketstudios.com
 * @since 0.1.0
 * @param microServices The service calling the log
 * @param traceId A trace ID for the inbound request
 * @param msg A lazily evaluated message.
 */
fun KLogger.trace(traceId: UUID, msg: () -> Any?) {
    if (isTraceEnabled) trace(buildLogMessage(traceId, msg))
}

/**
 * A info log helper that includes the calling *microService* and a *traceId*
 * @author sean.obrien@bottlerocketstudios.com
 * @since 0.1.0
 * @param microServices The service calling the log
 * @param traceId A trace ID for the inbound request
 * @param msg A lazily evaluated message.
 */
fun KLogger.info(traceId: UUID, msg: () -> Any?) {
    if (isInfoEnabled) info(buildLogMessage(traceId, msg))
}